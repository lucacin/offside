import grails.util.Environment
import com.offside.Footballer
import com.offside.RealTournament
import com.offside.RealTeam
import com.offside.RealMatchWeek

class BootStrap {
	String realTournamentCSVPath = "./web-app/datafiles/csv/real_tournaments.csv"
	String realMatchWeeksCSVPath = "./web-app/datafiles/csv/real_matchweeks.csv"
	String realTeamCSVPath = "./web-app/datafiles/csv/real_teams.csv"
	String footballerCSVPath = "./web-app/datafiles/csv/footballers.csv"

	def loadData() {
		if (RealTournament.count()==0) {
			log.info 'Loading real tournaments ...'
			def realTournamentCSV = new File(realTournamentCSVPath).splitEachLine(",") { fields ->
				log.debug """Loading Tournament (name: "${fields[0]}", year: "${fields[1]}")"""
				def newRealTournament = new RealTournament(name: fields[0].trim(), year: fields[1])
				newRealTournament.save(failOnError: true, flush: true)
			}
		}

		if (RealMatchWeek.count()==0) {
			log.info 'Loading real matchweeks ...'
			def realMatchWeeksCSV = new File(realMatchWeeksCSVPath).splitEachLine(",") { fields ->
				log.debug """Loading Matchweek (number: "${fields[0]}", init date: "${fields[1]}", FK: "${fields[2]}")"""
				def newRealMatchWeek = new RealMatchWeek(number: fields[0],initDate: Date.parse ("dd/MM/yyyy",fields[1].trim()))
				newRealMatchWeek.save(failOnError: true, flush: true)
				def foundRealTournament = RealTournament.where { name == "${fields[2]}"}.get()
				foundRealTournament.addToRealMatchWeeks(newRealMatchWeek)
			}
		}

		if (RealTeam.count() == 0) {
			log.info 'Loading real teams ...'
			def realTeamCSV = new File(realTeamCSVPath).splitEachLine(",") { fields ->
				log.debug """Loading RealTeam (name: "${fields[0]}", stadiumName: "${fields[1]}", FK: "${fields[2]}")"""
				def newRealTeam = new RealTeam(name: fields[0].trim(), stadiumName: fields[1].trim())
				newRealTeam.save(failOnError: true, flush: true)
				def foundRealTournament = RealTournament.where { name == "${fields[2]}"}.get()
				foundRealTournament.addToRealTeams(newRealTeam)
			}
		}
		if (Footballer.count() == 0) {
			log.info 'Loading footballers ...'
			def footballerCSV = new File(footballerCSVPath).splitEachLine(",") { fields ->
				log.debug """Loading Footballer (name: "${fields[0]}", position: "${fields[1]}", FK: "${fields[2]}")"""
				def newFootballer = new Footballer(name: fields[0].trim(), position: fields[1].trim())
				newFootballer.save(failOnError: true, flush: true)
				def foundRealTeam = RealTeam.where { name == "${fields[2]}"}.get()
				foundRealTeam.addToFootballers(newFootballer)
			}
		}
	}

	def init = { servletContext ->
		log.info "Bootstrap in environment type/name: ${Environment.current} / ${Environment.current.name}"
		environments {
			development_postgresql { this.loadData() }
			development {this.loadData()}
			mysql {this.loadData()}
		}
	}

	def destroy = {
	}
}
