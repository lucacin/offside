<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="layout" content="main">
<title><g:message code="forgotPassword.tab.title" /></title>
</head>
<body id="body">
	<h1>
		<g:message code="forgotPassword.header" />
	</h1>
	<p>
		<g:message code="forgotPassword.description" />
	</p>

	<!-- Forgot password errors -->
	<g:hasErrors>
		<div id="errorsPane" style="background-color: red;">
			<g:renderErrors bean="${forgotPasswordView}" as="list" />
		</div>
	</g:hasErrors>

	<div id="forgotPasswordPane">
		<g:form name="loginForm" controller="player" action="forgotPassword">
			<div id="selectPane">
				<p>
					<g:if test="${forgotPasswordSelected == 'login' || forgotPasswordSelected == null}">
						<input type="radio" name="forgotPasswordSelected" value="login"
							checked="checked" />
						<g:textField name="login" value="${forgotPasswordView?.login}" />
					</g:if>
					<g:else>
						<input type="radio" name="forgotPasswordSelected" value="login" />
						<g:textField name="login" value="" />
					</g:else>

					<label for="login"><g:message
							code="forgotPassword.label.login" /></label>
				</p>
				<p>
					<g:if test="${forgotPasswordSelected == 'email'}">
						<input type="radio" name="forgotPasswordSelected" value="email"
							checked="checked" />
						<g:textField name="email" value="${forgotPasswordView?.email}" />
					</g:if>
					<g:else>
						<input type="radio" name="forgotPasswordSelected" value="email" />
						<g:textField name="email" value="" />
					</g:else>
					<label for="email"><g:message
							code="forgotPassword.label.email" /></label>

				</p>

			</div>
			<div id="newPasswordPane">
				<p>
					<label for="password"><g:message
							code="forgotPassword.label.newPassword" /></label>
					<g:passwordField name="password" />
				</p>
				<p>
					<label for="confirmPassword"><g:message
							code="forgotPassword.label.confirmNewPassword" /></label>
					<g:passwordField name="confirmPassword" />
				</p>
			</div>
			<div id="buttonPane">
				<g:submitButton name="forgotBtn" value="Forgot" />
			</div>
		</g:form>
	</div>
</body>
</html>