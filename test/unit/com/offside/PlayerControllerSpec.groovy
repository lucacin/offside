package com.offside

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import spock.lang.Specification
import com.grailsinaction.LameSecurityFilters
import grails.test.mixin.TestFor
import grails.test.mixin.Mock



// automatically injects a controller
@TestFor(PlayerController)
@Mock(LameSecurityFilters)
class PlayerControllerSpec extends Specification {



	def "register:  Register a new player succesfully" (){

		given: "Register command correct data"

		def registerCommand = new RegisterCommand (login:"offside", email:"offside.dev@gmail.com", password:"offside_12",confirmPassword:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.register is executed only once and it is mocked to return Player
		def mockPlayer = new Player(registerCommand.properties)
		1 * mockPlayerService.register(_) >> mockPlayer
		controller.playerService = mockPlayerService

		when: "Register"

		withFilters (action:'register') { controller.register(registerCommand)}

		then: "Show OK message and redirect to login"

		flash.OKMessage == controller.message(code: "register.OK", args: ["${mockPlayer.login}"])
		flash.KOMessage == null
		flash.NotificationMessage == null
		response.redirectedUrl == "/"
	}

	def "register: Error validating new player" (){

		given: "Player command incorrect data"
		def registerCommand = new RegisterCommand (login:"offside", email:"offside.dev@gmail.com", password:"offside_12",confirmPassword:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.register is executed only once and it is mocked to return PlayerRegisterException
		def mockRegisterException = new PlayerRegisterException()
		1 * mockPlayerService.register(_) >> { throw mockRegisterException }
		controller.playerService = mockPlayerService

		when: "Register"

		withFilters (action:'register') { controller.register(registerCommand)}

		then: "Show again the register form"

		response.redirectedUrl == null
	}


	def "register: Error saving new player" (){

		given: "Player command incorrect saving data"
		def registerCommand = new RegisterCommand (login:"offside", email:"offside.dev@gmail.com", password:"offside_12",confirmPassword:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.register is executed only once and it is mocked to return PlayerException
		def mockPlayerException = new PlayerException()
		1 * mockPlayerService.register(_) >> { throw mockPlayerException }
		controller.playerService = mockPlayerService

		when: "Register"
		withFilters (action:'register') { controller.register(registerCommand) }

		then: "Show again the register form"

		response.redirectedUrl == null
	}

	def "register (filter): KO authenticated access"() {
		given: "Authentication access"
		def registerCommand = new RegisterCommand (login:"offside", email:"offside.dev@gmail.com", password:"offside_12",confirmPassword:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.register is not executed because controller.register is not executed either due to the filter
		def mockPlayer = new Player(registerCommand.properties)
		0 * mockPlayerService.register(_) >> mockPlayer
		controller.playerService = mockPlayerService

		def securityFilters = new LameSecurityFilters()
		securityFilters.setAuthentication(session,mockPlayer)

		when:
		withFilters (action:'register') { controller.register(registerCommand) }

		then:
		securityFilters.isAuthenticatedAccess(session) == true
		response.redirectedUrl == '/player/home'
	}

	def "login: Login succesfully"() {
		given: "Registered player"

		def loginCommand = new LoginCommand (login:"offside", password:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.login is executed only once and it is mocked to return Player
		def mockPlayer = new Player(loginCommand.properties)
		1 * mockPlayerService.login(_) >> mockPlayer
		controller.playerService = mockPlayerService

		when: "Login"

		withFilters (action:'login'){controller.login(loginCommand)}

		then: "User is logged in"
		def securityFilters = new LameSecurityFilters()
		securityFilters.isAuthenticatedAccess(session) == true
		response.redirectedUrl =='/player/home'
	}

	def "login: Login error"() {
		given: "Not registered player"

		def loginCommand = new LoginCommand (login:"offside", password:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.login is executed only once and it is mocked to return mockPlayer
		def mockPlayerLoginException = new PlayerLoginException()
		1 * mockPlayerService.login(_) >> { throw mockPlayerLoginException }
		controller.playerService = mockPlayerService

		when: "Login"

		withFilters (action:'login') {controller.login(loginCommand)}

		then: "Show the login form again"

		response.redirectedUrl == null
	}
	def "login (filter): KO, authenticated user"() {


		given: "Authentication access"

		def loginCommand = new LoginCommand (login:"offside", password:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.login is not executed due to the filter
		def mockPlayer = new Player(loginCommand.properties)
		0 * mockPlayerService.login(_) >> mockPlayer
		controller.playerService = mockPlayerService

		// Authenticated access
		def securityFilters = new LameSecurityFilters()
		securityFilters.setAuthentication(session,mockPlayer)

		when:
		withFilters (action:'login') { controller.login(loginCommand) }

		then:
		securityFilters.isAuthenticatedAccess(session) == true
		response.redirectedUrl == '/player/home'
	}

	def "forgotPassword: forgotPassword by login OK"() {
		given: "Forgot password command"
		def forgotPasswordCommand = new ForgotPasswordCommand (login:"offside", email:"", password:"offside_12",confirmPassword:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.forgotPasswordByLogin is executed only once and it is mocked to return mockPlayer
		def mockPlayer = new Player(forgotPasswordCommand.properties)
		mockPlayer.email="offside.dev@gmail.com"
		params.forgotPasswordSelected="login"
		1 * mockPlayerService.forgotPasswordByLogin(_) >> mockPlayer
		controller.playerService = mockPlayerService

		when :	"forgotPassword"

		withFilters (action:'forgotPassword') {controller.forgotPassword(forgotPasswordCommand)}

		then:"redirected lo login"
		flash.OKMessage == controller.message(code: "forgotPassword.OK", args: [mockPlayer.login])
		flash.KOMessage == null
		flash.NotificationMessage == null
		response.redirectedUrl == '/'
	}

	def "forgotPassword: forgotPassword by email OK"() {
		given: "Forgot password command"
		def forgotPasswordCommand = new ForgotPasswordCommand (login:"", email:"offside.dev@gmail.com", password:"offside_12",confirmPassword:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.forgotPasswordByEmail is executed only once and it is mocked to return mockPlayer
		def mockPlayer = new Player(forgotPasswordCommand.properties)
		mockPlayer.login="offside"
		params.forgotPasswordSelected="email"
		1 * mockPlayerService.forgotPasswordByEmail(_) >> mockPlayer
		controller.playerService = mockPlayerService

		when :	"forgotPassword"

		withFilters (action:'forgotPassword') {controller.forgotPassword(forgotPasswordCommand)}

		then:"redirected lo login"
		flash.OKMessage == controller.message(code: "forgotPassword.OK", args: [mockPlayer.login])
		flash.KOMessage == null
		flash.NotificationMessage == null

		response.redirectedUrl == '/'
	}

	def "forgotPassword: forgotPassword by login errors"() {
		given: "Forgot password command"
		def forgotPasswordCommand = new ForgotPasswordCommand (login:"offside", email:"", password:"offside_12",confirmPassword:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.forgotPasswordByLogin is executed only once and it is mocked to return PlayerForgotPasswordException
		def mockPlayerForgotPasswordException = new PlayerForgotPasswordException()
		params.forgotPasswordSelected="login"
		1 * mockPlayerService.forgotPasswordByLogin(_) >> { throw mockPlayerForgotPasswordException}
		controller.playerService = mockPlayerService

		when :	"forgotPassword"

		withFilters (action:'forgotPassword') {controller.forgotPassword(forgotPasswordCommand)}

		then:"not redirected"

		response.redirectedUrl == null
	}

	def "forgotPassword: forgotPassword by email errors"() {
		given: "Forgot password command"
		def forgotPasswordCommand = new ForgotPasswordCommand (login:"", email:"offside.dev@gmail.com", password:"offside_12",confirmPassword:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.forgotPasswordByLogin is executed only once and it is mocked to return PlayerForgotPasswordException
		def mockPlayerForgotPasswordException = new PlayerForgotPasswordException()
		params.forgotPasswordSelected="email"
		1 * mockPlayerService.forgotPasswordByEmail(_) >> { throw mockPlayerForgotPasswordException}
		controller.playerService = mockPlayerService

		when :	"forgotPassword"

		withFilters (action:'forgotPassword') {controller.forgotPassword(forgotPasswordCommand)}

		then:"not redirected"

		response.redirectedUrl == null
	}

	def "forgotPassword (filter): KO, authenticated users"() {
		given: "Authenticated access"

		def forgotPasswordCommand = new ForgotPasswordCommand (login:"offside", email:"", password:"offside_12",confirmPassword:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.forgotPasswordByLogin is not executed due to the filter
		def mockPlayer = new Player(forgotPasswordCommand.properties)
		mockPlayer.email="offside.dev@gmail.com"
		params.forgotPasswordSelected="login"
		0 * mockPlayerService.forgotPasswordByLogin(_) >> mockPlayer
		0 * mockPlayerService.forgotPasswordByEmail(_) >> mockPlayer
		controller.playerService = mockPlayerService

		// Authenticated access
		def securityFilters = new LameSecurityFilters()
		securityFilters.setAuthentication(session,mockPlayer)

		when :	"forgotPassword"

		withFilters (action:'forgotPassword') {controller.forgotPassword(forgotPasswordCommand)}

		then:"redirected lo login"
		securityFilters.isAuthenticatedAccess(session) == true
		response.redirectedUrl == '/player/home'
	}


	def "logout: OK"() {
		given: "Authenticated access"
		def mockPlayer = new Player (login:"offside", email:"offside.dev@gmail.com")
		def securityFilters = new LameSecurityFilters()
		securityFilters.setAuthentication(session,mockPlayer)

		when :	"logout"
		withFilters (action:'logout') {controller.logout()}

		then:"redirected"

		securityFilters.isAuthenticatedAccess(session) == false
		flash.OKMessage == controller.message(code: "player.logout.ok", args: [mockPlayer.login])
		flash.KOMessage == null
		flash.NotificationMessage == null
		response.redirectedUrl == '/'

	}

	def "logout (filter): KO, unauthenticated access"() {
		given: "Unauthenticated access"
		def mockPlayer = new Player (login:"offside", email:"offside.dev@gmail.com")
		def securityFilters = new LameSecurityFilters()

		when :	"logout"
		withFilters (action:'logout') { controller.logout()}

		then:"redirected"

		securityFilters.isAuthenticatedAccess(session) == false
		flash.OKMessage == null
		flash.KOMessage == controller.message(code: "error.notauthenticated.player")
		flash.NotificationMessage == null
		response.redirectedUrl == '/'

	}


}
