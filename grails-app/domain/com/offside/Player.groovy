package com.offside
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.lang.RandomStringUtils

class Player {

	String login
	String email
	String passwordHash

	Profile profile

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp


	//FIXME: belongsTo implies delete in cascade
	static belongsTo=Tournament
	static hasMany=[teams:Team,tournaments:Tournament]

	static constraints = {
		login(blank:false,size: 4..12,unique:true)
		email(blank:false,email:true,unique:true)
		passwordHash(blank:false)
		profile(nullable: true)
	}

	static scaffold = true

	static mapping = { profile lazy:false }

	public void setPasswordToHash (String clearPassword) {
		this.passwordHash=DigestUtils.md5Hex(clearPassword)
	}
	
	public boolean checkPassword (String clearPassword) {
		if (this.passwordHash==null)
			return false
		return DigestUtils.md5Hex(clearPassword) == this.passwordHash
	}
	

	@Override
	public String toString() {
		return "Player [login=" + login + ", email=" + email + ", passwordHash=" + passwordHash + "]"
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Player)) {
			return false;
		}
		Player other = (Player) obj;
		if (login == null) {
			if (other.login != null) {
				return false;
			}
		} else if (!login.equalsIgnoreCase(other.login)) {
			return false;
		}
		return true;
	}


}
