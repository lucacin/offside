dataSource {
	pooled = true
	driverClassName = "org.h2.Driver"
	username = "sa"
	password = ""
}
hibernate {
	cache.use_second_level_cache = true
	cache.use_query_cache = false
	cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
	development {
		dataSource {
			dbCreate = "create-drop"
			url = "jdbc:mysql://localhost:3306/offside"
			driverClassName = "com.mysql.jdbc.Driver"
			dialect ="org.hibernate.dialect.MySQLDialect"
			username = "root"
			password = ""
		}/*
		 dataSource {
		 dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
		 url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
		 }*/
	}
	test {
		dataSource {
			dbCreate = "create-drop"
			url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
		}
	}
	production {
		//heroku
		dataSource {
			dbCreate = "update"
			driverClassName = "org.postgresql.Driver"
			dialect = org.hibernate.dialect.PostgreSQLDialect

			uri = new URI(System.env.DATABASE_URL?:"postgres://test:test@localhost/test")

			url = "jdbc:postgresql://"+uri.host+uri.path
			username = uri.userInfo.split(":")[0]
			password = uri.userInfo.split(":")[1]
		}
	}

	mysql{
		dataSource {
			dbCreate = "create-drop"
			url = "jdbc:mysql://localhost:3306/offside"
			driverClassName = "com.mysql.jdbc.Driver"
			dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
			//dialect ="org.hibernate.dialect.MySQLDialect"
			username = "root"
			password = ""
		}

	}

	// postgresql 
	development_postgresql {

		dataSource {
			pooled = true
			//dbCreate = "create-drop"
			dbCreate = "update"
			driverClassName = "org.postgresql.Driver"
			dialect = org.hibernate.dialect.PostgreSQLDialect
			url = "jdbc:postgresql://localhost:5432/offside"
			username = "offside"
			password = "laliganauer"
		}

	}
}
