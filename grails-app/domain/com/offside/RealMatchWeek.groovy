package com.offside

class RealMatchWeek {

	Integer number
	Date initDate
	
	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp
	
	//FIXME: belongsTo implies delete in cascade	
	//static belongsTo =[realTournament:RealTournament]

	static hasMany=[lineups:Lineup,scores:Score]
   static constraints = {
		number (blank:false)
		initDate (blank:false)
   }

	static scaffold = true

	String toString() {
		return '''("${this.number}","${this.initDate}")'''
	}
}
