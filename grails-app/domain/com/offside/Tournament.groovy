package com.offside

class Tournament {
	
	String name
	String description

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp

	static hasMany=[offsideTeams:Team,offsideUsers:Player]
    
	static constraints = {
		name (blank: false, unique:true, maxSize: 20)
		description (nullable:true, maxSize: 100)
    }
	
	static scaffold = true

	String toString()  {
		return "${name}"
	}
}
