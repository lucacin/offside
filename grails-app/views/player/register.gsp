<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="layout" content="main">
<title><g:message code="register.tab.title" /></title>
</head>


<body id="body">
	<h1>
		<g:message code="register.page.header" />
	</h1>

	<p>
		<g:message code="register.page.desc" />
	</p>
	
	<g:hasErrors>
		<div id="errorsPane" style="background-color: red;">
			<g:renderErrors bean="${playerView}" as="list" />
		</div>
	</g:hasErrors>

	<div id="registerPane">
		<g:form controller="player" action="register" name="registerForm">
			<div class="formField">
				<label for="login"><g:message
						code="register.form.label.login" /></label>
				<g:textField name="login" value="${playerView?.login}" />
			</div>

			<div class="formField">
				<label for="email"><g:message
						code="register.form.label.email" /></label>
				<g:textField name="email" value="${playerView?.email}" />
			</div>
			<div class="formField">
				<label for="password"><g:message
						code="register.form.label.password" /></label>
				<g:passwordField name="password" />
			</div>
			<div class="formField">
				<label for="confirmPassword"><g:message
						code="register.form.label.confirmPassword" /></label>
				<g:passwordField name="confirmPassword" />
			</div>
			<g:submitButton class="formButton" name="register" value="Register" />
		</g:form>
	</div>
</body>
</html>