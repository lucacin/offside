package com.offside;

class PlayerException extends RuntimeException {
	String message
	Player player
}

class PlayerRegisterException extends PlayerException {
	String message
	RegisterCommand registerCommand
}

class PlayerLoginException extends RuntimeException {
	String message
	LoginCommand loginCommand
}

class PlayerForgotPasswordException extends RuntimeException {
	String message
	ForgotPasswordCommand forgotPasswordCommand
}


class PlayerService {
	// Grails automatically sets service transactional
	static transactional = true

	def Player login (LoginCommand loginCommand) throws PlayerLoginException  {

		if (loginCommand.validate()) {
			def foundPlayer = Player.findByLogin(loginCommand.login)
			if (foundPlayer) {
				if (foundPlayer.checkPassword(loginCommand.password)) {
					return foundPlayer
				}
				else {
					//FIXME: Me gustaria asociar el error al campo pero no me reconoce la signatura
					// http://static.springsource.org/spring/docs/1.2.9/api/org/springframework/validation/Errors.html
					loginCommand.errors.reject('login.player.password.incorrect',["${loginCommand.login}"] as Object[],"Default incorrect password message" )
					throw new PlayerLoginException (message:"Password incorrect", loginCommand: loginCommand)
				}
			}
			else {
				//FIXME: Me gustaria asociar el error al campo pero no me reconoce la signatura
				// http://static.springsource.org/spring/docs/1.2.9/api/org/springframework/validation/Errors.html
				loginCommand.errors.reject('login.player.not.found',["${loginCommand.login}"] as Object[],"Default Login not found message" )
				throw new PlayerLoginException (message:"Player not found", loginCommand: loginCommand)
			}
		}
		else {
			throw new PlayerLoginException (message:"Login Command Validation", loginCommand: loginCommand)
		}

	}

	def Player register (RegisterCommand registerCommand) throws PlayerRegisterException, PlayerException {
		def player = new Player(registerCommand.properties)

		if (registerCommand.validate()) {
			player.setPasswordToHash(registerCommand.password)
			// Automatically validate all player fields
			if (player.save()) {
				return player
			}
			else {
				throw new PlayerException (message:"Player Save Exception", player: player)
			}
		}
		else {
			throw new PlayerRegisterException (message:"Register Command validation",registerCommand: registerCommand)
		}
	}

	def Player forgotPasswordByLogin (ForgotPasswordCommand forgotPasswordCommand) throws PlayerForgotPasswordException,PlayerException {
		def player = new Player(forgotPasswordCommand.properties)

		if (forgotPasswordCommand.validate([
			'login',
			'password',
			'confirmPassword'
		])) {
			def foundPlayer = Player.findByLogin(player.login)
			if (foundPlayer) {
				//TODO: sent confirmation link by mail
				foundPlayer.setPasswordToHash(forgotPasswordCommand.password)
				if (foundPlayer.save()){
					return foundPlayer
				}
				else {
					throw new PlayerException (message:"Save error: PlayerException", player: foundPlayer)
				}
			}
			else {
				forgotPasswordCommand.errors.reject("forgotPassword.player.login.not.found",["${player.login}"] as Object[],"Default login not found message" )
				throw new PlayerForgotPasswordException (message:"Player not found", forgotPasswordCommand: forgotPasswordCommand)
			}
		}
		else {
			throw new PlayerForgotPasswordException (message:"Player ForgotPassword Exception (login)", forgotPasswordCommand: forgotPasswordCommand)
		}
	}

	def Player forgotPasswordByEmail (ForgotPasswordCommand forgotPasswordCommand) throws PlayerForgotPasswordException,PlayerException {

		def player = new Player(forgotPasswordCommand.properties)

		if (forgotPasswordCommand.validate([
			"email",
			"password",
			"confirmPassword"
		])) {
			def foundPlayer = Player.findByEmail(player.email)
			if (foundPlayer) {
				//TODO: sent confirmation link by mail
				foundPlayer.setPasswordToHash(forgotPasswordCommand.password)
				if (foundPlayer.save()){
					return foundPlayer
				}
				else {
					throw new PlayerException (message:"Save error: PlayerException", player: foundPlayer)
				}
			}
			else {
				forgotPasswordCommand.errors.reject("forgotPassword.player.email.not.found",["${player.email}"] as Object[],"Default email not found message" )
				throw new PlayerForgotPasswordException (message:"Player not found", forgotPasswordCommand: forgotPasswordCommand)
			}
		}
		else {
			throw new PlayerForgotPasswordException (message:"Player ForgotPassword Exception (email)", forgotPasswordCommand: forgotPasswordCommand)
		}

	}

	def serviceMethod() {
	}
}
