package com.offside

enum Position {
	GOALKEEPER,DEFENDER,MIDFIELDER,STRIKER
}

class Footballer {


	String name
	byte[] photo
	Position position
	Integer value

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp

	//FIXME: belongsTo implies delete in cascade
	//static belongsTo =[Team, RealTeam, Lineup]
	static belongsTo =[Team, Lineup]

	static hasMany =[teams:Team,lineups:Lineup,scores:Score]


	static constraints = {
		name (blank:false)
		position (blank:false)
		photo (nullable:true)
		value (nullable:true)
	}

	static scaffold = true

	String toString() {
		return "${name}"
	}

}
