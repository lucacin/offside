package com.offside

class TournamentController {

   def scaffold = Tournament
   
   
   def createTournament()
   {
	   if (Tournament.findAll().size()==0) 
		   {
			   def Tournament newTournament=new Tournament()
			   newTournament.name="TestTournament"
			   newTournament.description="TestTournamentDescription"
			   if( ! newTournament.save(flush: true)) {
				   newTournament.errors.each {
						 println it
					}
				 }
		   }
   }
   
   def list(Integer max)
   {
	   createTournament()
	   params.max = Math.min(max ?: 10, 100)
	   [tournamentInstanceList: Tournament.list(params), tournamentInstanceTotal: Tournament.count()]
   }
}
