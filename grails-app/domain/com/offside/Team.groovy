package com.offside

class Team {
	
	String name

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp


	//FIXME: belongsTo implies delete in cascade	
	//static belongsTo =[tournament:Tournament,player:Player]

	static hasMany =[footballers:Footballer,lineups:Lineup]
	//TODO: Why fetchmode: 'eager'
	static fetchMode = [footballers: 'eager']
	
    static constraints = {
		name(blank:false,unique:true,maxSize: 20)
    }

	static scaffold = true

	String toString() {
		return "${name}"
	}

}
