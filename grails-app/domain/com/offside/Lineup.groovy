package com.offside

class Lineup {

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp

//FIXME: belongsTo implies delete in cascade
//static belongsTo =[matchWeek:RealMatchWeek,team:Team]

	static hasMany=[footballers:Footballer]
   static constraints = {
    }

	static scaffold = true
}
