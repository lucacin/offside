package com.offside

class FootballerController {

      def scaffold = Footballer
	
	def loadFromFile()
	{
		 String[] lines= new File('web-app/datafiles/footballPlayers.txt').text.split('\n')
		 
		  List<String[]> footballPlayers = lines.collect {it.split(';')}
		  
		  for ( footballPlayer in footballPlayers ) {
			   def footballPlayerInstance = new Footballer()
		
		footballPlayerInstance.setName("")
		  footballPlayerInstance.setNickname(footballPlayer[1])
		  footballPlayerInstance.setValue(0)
	 
			   
		 switch (new Integer(footballPlayer[2]))
		 {
			 case 1:
			 footballPlayerInstance.setPosition(Footballer.Position.GOALKEEPER);
			 break
			 case 2:
			 footballPlayerInstance.setPosition(Footballer.Position.DEFENDER);
			 break
			 case 3:
			 footballPlayerInstance.setPosition(Footballer.Position.MIDFIELDER);
			 break
			 case 4:
			 footballPlayerInstance.setPosition(Footballer.Position.STRIKER);
			 break
			 }
			
		 RealTeam realTeam=RealTeam.get(new Integer(footballPlayer[4]))
		  
		 realTeam.footballers.addAll(footballPlayerInstance)
		  if( ! footballPlayerInstance.save(flush: true)) {
			 footballPlayerInstance.errors.each {
				   println it
			  }
		   }
		  }
		  
	 
	  
	}
	
	def list(Integer max)
	{   
		if (Footballer.count()==0)
		{
		loadFromFile()
		}
		params.max = Math.min(max ?: 10, 100)
		[footballerInstanceList: Footballer.list(params), footballerInstanceTotal: Footballer.count()]
	}
	
	def index= {
		
			redirect(action: "list", params: params)
	}
}
