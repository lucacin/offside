package com.offside

import spock.lang.*
import grails.plugin.spock.*
import spock.lang.Specification
import grails.test.mixin.TestFor
import grails.test.mixin.Mock


// TODO : validate error messages (no se si deberia hacerse cuando probamos el service, supongo que si)
class PlayerServiceIntegrationSpec extends IntegrationSpec {

	def "PlayerService.register:  Register a new player succesfully" (){

		def playerService = new PlayerService()

		given: "Register command correct data"

		def registerCommand = new RegisterCommand (login:"offside", email:"offside.dev@gmail.com", password:"offside_12",confirmPassword:"offside_12")

		when: "Register"

		def player=playerService.register(registerCommand)

		then: "Registered in DB"

		player.login==registerCommand.login
		player.email == registerCommand.email
		player.checkPassword(registerCommand.password) == true
		player.profile == null

		Player.findByLogin(registerCommand.login)==player
	}

	@Unroll
	def "PlayerService.register:  Constraint validations: field: #field, constraint: #exceptionCode"() {
		given: "Register command non validatable objects"

		def playerService = new PlayerService()

		def registerCommand = new RegisterCommand (login:login,email:email,password:password,confirmPassword:confirmPassword)

		when:"Register"

		def player=playerService.register(registerCommand)

		then:
		PlayerRegisterException playerRegisterException = thrown()

		def errors = playerRegisterException.registerCommand.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == exceptionCode


		where:
		field|login|email|password|confirmPassword|exceptionCode
		'login'|null|'offside@gmail.com'|'offside'|'offside'|'nullable'
		'login'|''|'offside@gmail.com'|'offside'|'offside'|'blank'
		'login'|'off'|'offside@gmail.com'|'offside'|'offside'|'size.toosmall'
		'login'|'offside012345'|'offside@gmail.com'|'offside'|'offside'|'size.toobig'
		'email'|'offside'|null|'offside'|'offside'|'nullable'
		'email'|'offside'|''|'offside'|'offside'|'blank'
		'email'|'offside'|'offside@gmail'|'offside'|'offside'|'email.invalid'
		'password'|'offside'|'offside@gmail.com'|null|'offside'|'nullable'
		'password'|'offside'|'offside@gmail.com'|''|'offside'|'blank'
		'password'|'offside'|'offside@gmail.com'|'offsi'|'offside'|'size.toosmall'
		'password'|'offside'|'offside@gmail.com'|'offside012345'|'offside'|'size.toobig'
		'confirmPassword'|'offside'|'offside@gmail.com'|'offside'|null|'nullable'
		'confirmPassword'|'offside'|'offside@gmail.com'|'offside'|''|'blank'
		'confirmPassword'|'offside'|'offside@gmail.com'|'offside'|'OFFSIDE'|'validator.invalid'
	}

	@Unroll
	def "PlayerService.register:  Unique validations: field: #field, constraint: #exceptionCode"() {
		given: "Register command non validatable objects"

		def playerService = new PlayerService()
		def password = 'offside2'

		def registerCommand = new RegisterCommand (login:login,email:email,password:password,confirmPassword: password)

		def registerCommand2 = new RegisterCommand (login:login2,email:email2,password:password,confirmPassword: password)

		when:"Register"

		def player=playerService.register(registerCommand)
		def player2=playerService.register(registerCommand2)

		then:
		PlayerException playerException = thrown()

		def errors = playerException.player.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == exceptionCode


		where:
		field|login|email|login2|email2|exceptionCode
		'login'|'offside'|'offside@gmail.com'|'offside'|'liganauer@gmail.com'|'unique'
		'email'|'offside'|'offside@gmail.com'|'liganauer'|'offside@gmail.com'|'unique'
	}

	def "PlayerService.login:  Login succesfully" (){

		def playerService = new PlayerService()

		given: "Login command correct data"
		def login = 'offside'
		def password = 'offside2'

		def registerCommand = new RegisterCommand (login:login, email:'offside.dev@gmail.com', password:password,confirmPassword:password)
		def loginCommand = new LoginCommand (login:login, password: password)

		when: "Register and then login"

		playerService.register(registerCommand)
		def player=playerService.login(loginCommand)

		then: "Login validates player"

		player.login==loginCommand.login
		player.checkPassword(loginCommand.password) == true
	}

	@Unroll
	def "PlayerService.login:  Constraint validations: field: #field, constraint: #exceptionCode"() {
		given: "Login command non validatable objects"

		def playerService = new PlayerService()

		def loginCommand = new LoginCommand (login: login,password: password)

		when:"login"

		def player=playerService.login(loginCommand)

		then: "Login player validate errors"
		PlayerLoginException playerLoginException = thrown()

		def errors = playerLoginException.loginCommand.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == exceptionCode


		where:
		field|login|password|exceptionCode
		'login'|null|'offside12'|'nullable'
		'login'|''|'offside12'|'blank'
		'login'|'off'|'offsid12'|'size.toosmall'
		'login'|'offside012345'|'offside12.com'|'size.toobig'
		'password'|'offside'|null|'nullable'
		'password'|'offside'|''|'blank'
		'password'|'offside'|'offsi'|'size.toosmall'
		'password'|'offside'|'offside012345'|'size.toobig'
	}


	def "PlayerService.login:  Player not found"() {
		given: "Incorrect login field in login command"

		def playerService = new PlayerService()
		String login = 'offside'
		def password = 'offside12'

		def registerCommand = new RegisterCommand (login:login, email:'offside.dev@gmail.com', password:password,confirmPassword:password)
		def loginCommand = new LoginCommand (login: login.toUpperCase(),password: password)

		when:"login"

		playerService.register(registerCommand)
		def player=playerService.login(loginCommand)

		then: "Login player validate errors"
		PlayerLoginException playerLoginException = thrown()

		def errors = playerLoginException.loginCommand.errors

		errors.hasGlobalErrors() == true
		errors.getGlobalErrorCount() == 1
		errors.getGlobalError().getCode() == 'login.player.not.found'
	}

	def "PlayerService.login:  Incorrect password"() {
		given: "Incorrect password"

		def playerService = new PlayerService()
		String login = 'offside'
		def password = 'offside12'

		def registerCommand = new RegisterCommand (login:login, email:'offside.dev@gmail.com', password:password,confirmPassword:password)
		def loginCommand = new LoginCommand (login: login,password: password.toUpperCase())

		when:"login"

		playerService.register(registerCommand)
		def player=playerService.login(loginCommand)

		then: "Login player validate errors"
		PlayerLoginException playerLoginException = thrown()

		def errors = playerLoginException.loginCommand.errors

		errors.hasGlobalErrors() == true
		errors.getGlobalErrorCount() == 1
		errors.getGlobalError().getCode() == 'login.player.password.incorrect'
	}

	def "PlayerService.login:  Incorrect password"() {
		given: "Incorrect password"

		def playerService = new PlayerService()
		String login = 'offside'
		def password = 'offside12'

		def registerCommand = new RegisterCommand (login:login, email:'offside.dev@gmail.com', password:password,confirmPassword:password)
		def loginCommand = new LoginCommand (login: login,password: password.toUpperCase())

		when:"login"

		playerService.register(registerCommand)
		def player=playerService.login(loginCommand)

		then: "Login player validate errors"
		PlayerLoginException playerLoginException = thrown()

		def errors = playerLoginException.loginCommand.errors

		errors.hasGlobalErrors() == true
		errors.getGlobalErrorCount() == 1
		errors.getGlobalError().getCode() == 'login.player.password.incorrect'
	}


	def "PlayerService.forgotPasswordByLogin success"() {
		given: "Correct forgotPassword data"

		def playerService = new PlayerService()

		def login = ' offside'
		def password = 'offside.12'
		def newPassword = 'liganauer2'

		def registerCommand = new RegisterCommand (login:login, email:'offside.dev@gmail.com', password:password,confirmPassword:password)
		def forgotPasswordCommand = new ForgotPasswordCommand(login: login,password: newPassword,confirmPassword:newPassword)


		when: "set a new password"

		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByLogin(forgotPasswordCommand)

		then: "password is changed"

		player.login == registerCommand.login
		player.checkPassword(newPassword) == true
		
		Player playerFound = Player.findByLogin(login)
		playerFound.checkPassword(newPassword) == true
	}

	@Unroll
	def "PlayerService.forgotPasswordByLogin constraint validations: field: #field, constraint: #exceptionCode"() {
		given: "Incorrect forgotPassword data"

		def playerService = new PlayerService()
		def loginRegister = 'offside'
		def passwordRegister = 'orsay.16'
		def registerCommand = new RegisterCommand (login:loginRegister, email:'offside.dev@gmail.com', password:passwordRegister,confirmPassword:passwordRegister)
		def forgotPasswordCommand = new ForgotPasswordCommand(login: login,password: newPassword,confirmPassword:newConfirmPassword)

		when: "try to set a new password"
		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByLogin(forgotPasswordCommand)

		then: "Exception is thrown and password is not changed"
		PlayerForgotPasswordException playerForgotPasswordException = thrown()

		def errors = playerForgotPasswordException.forgotPasswordCommand.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == exceptionCode

		Player playerFound = Player.findByLogin(loginRegister)
		playerFound.checkPassword(passwordRegister) == true

		where:
		field|login|newPassword|newConfirmPassword|exceptionCode
		'login'|null|'offside12'|'offside12'|'nullable'
		'login'|''|'offside12'|'offside12'|'blank'
		'login'|'off'|'offsid12'|'offside12'|'size.toosmall'
		'login'|'offside012345'|'offside12'|'offside12'|'size.toobig'
		'password'|'offside'|null|'offside12'|'nullable'
		'password'|'offside'|''|'offside12'|'blank'
		'password'|'offside'|'offsi'|'offside12'|'size.toosmall'
		'password'|'offside'|'offside012345'|'offside12'|'size.toobig'
		'confirmPassword'|'offside'|'offside12'|null|'nullable'
		'confirmPassword'|'offside'|'offside12'|''|'blank'
		'confirmPassword'|'offside'|'offside_12'|'OFFSIDE_12'|'validator.invalid'
	}

	def "PlayerService.forgotPasswordByLogin : Player not found"() {
		given: "Incorrect forgotPassword data"

		def playerService = new PlayerService()
		def login = 'offside'
		def password = 'offs1d3'
		def newPassword = 'offs1d3.12'

		def registerCommand = new RegisterCommand (login:login, email:'offside.dev@gmail.com', password:password,confirmPassword:password)
		def forgotPasswordCommand = new ForgotPasswordCommand(login: login.toUpperCase(),password: newPassword,confirmPassword:newPassword)

		when: "try to set a new password"
		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByLogin(forgotPasswordCommand)

		then: "Exception is thrown"
		PlayerForgotPasswordException playerForgotPasswordException = thrown()

		def errors = playerForgotPasswordException.forgotPasswordCommand.errors
		errors.hasGlobalErrors() == true
		errors.getGlobalErrorCount() == 1
		errors.getGlobalError().getCode() == 'forgotPassword.player.login.not.found'
	}


	def "PlayerService.forgotPasswordByEmail success"() {
		given: "Correct forgotPassword data"

		def playerService = new PlayerService()

		def login = ' offside'
		def email = 'offside.dev@gmail.com'
		def password = 'offside.12'
		def newPassword = 'liganauer2'

		def registerCommand = new RegisterCommand (login:login, email:email, password:password,confirmPassword:password)
		def forgotPasswordCommand = new ForgotPasswordCommand(email: email,password: newPassword,confirmPassword:newPassword)


		when: "set a new password"

		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByEmail(forgotPasswordCommand)

		then: "password is changed"

		player.login == registerCommand.login
		player.email==forgotPasswordCommand.email
		player.checkPassword(newPassword) == true
		
		Player playerFound = Player.findByEmail(email)
		playerFound.checkPassword(newPassword) == true
		
	}

	@Unroll
	def "PlayerService.forgotPasswordByEmail constraint validations: field: #field, constraint: #exceptionCode"() {
		given: "Incorrect forgotPassword data"

		def playerService = new PlayerService()
		def loginRegister = 'offside'
		def emailRegister = 'offside.dev@gmail.com'
		def passwordRegister = 'orsay.16'

		def registerCommand = new RegisterCommand (login:loginRegister, email:emailRegister, password:passwordRegister,confirmPassword:passwordRegister)
		def forgotPasswordCommand = new ForgotPasswordCommand(email: email,password: newPassword,confirmPassword:newConfirmPassword)

		when: "try to set a new password"
		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByEmail(forgotPasswordCommand)

		then: "Exception is thrown and password is not changed"
		PlayerForgotPasswordException playerForgotPasswordException = thrown()

		def errors = playerForgotPasswordException.forgotPasswordCommand.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == exceptionCode

		Player playerFound = Player.findByEmail(emailRegister)
		playerFound.checkPassword(passwordRegister) == true

		where:
		field|email|newPassword|newConfirmPassword|exceptionCode
		'email'|null|'offside12'|'offside12'|'nullable'
		'email'|''|'offside12'|'offside12'|'blank'
		'email'|'offside.dev@gmail'|'offside12'|'offside12'|'email.invalid'
		'password'|'offside.dev@gmail.com'|null|'offside12'|'nullable'
		'password'|'offside.dev@gmail.com'|''|'offside12'|'blank'
		'password'|'offside.dev@gmail.com'|'offsi'|'offside12'|'size.toosmall'
		'password'|'offside.dev@gmail.com'|'offside012345'|'offside12'|'size.toobig'
		'confirmPassword'|'offside.dev@gmail.com'|'offside12'|null|'nullable'
		'confirmPassword'|'offside.dev@gmail.com'|'offside12'|''|'blank'
		'confirmPassword'|'offside.dev@gmail.com'|'offside_12'|'OFFSIDE_12'|'validator.invalid'
	}

	def "PlayerService.forgotPasswordByEmail : Player not found"() {
		given: "Incorrect forgotPassword data"

		def playerService = new PlayerService()
		def login = 'offside'
		def email = 'offside.dev@gmail.com'
		def password = 'offs1d3'
		def newPassword = 'offs1d3.12'

		def registerCommand = new RegisterCommand (login:login, email:email, password:password,confirmPassword:password)
		def forgotPasswordCommand = new ForgotPasswordCommand(email: email.toUpperCase(),password: newPassword,confirmPassword:newPassword)

		when: "try to set a new password"
		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByEmail(forgotPasswordCommand)

		then: "Exception is thrown"
		PlayerForgotPasswordException playerForgotPasswordException = thrown()

		def errors = playerForgotPasswordException.forgotPasswordCommand.errors
		errors.hasGlobalErrors() == true
		errors.getGlobalErrorCount() == 1
		errors.getGlobalError().getCode() == 'forgotPassword.player.email.not.found'
	}




}
