package com.offside

class Score {

	Integer points

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp

	static belongsTo =[footballer:Footballer,realMatchWeek:RealMatchWeek]
	
   static constraints = {
		points (blank:false)
   }

	static scaffold = true
}
