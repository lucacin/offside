package com.offside

class TeamController {

	def scaffold = Team
	
	def Player userInSession
	
	def createRandomTeam()
	{
		def int totalFootballers=Footballer.count
		def int footballerNum
		def int teamDefaultSize=15
		
		userInSession=Player.findWhere(email:session.user.email)
		if (userInSession!=null)
			{
			if (userInSession.offsideTeams==null || userInSession.offsideTeams.size()==0)
				{
					def Team newTeam=new Team()
					def Tournament newTournament=Tournament.first()
					newTeam.name="TestTeam"+ (new Date()).toString()
					newTeam.offsideLeague=newTournament
					
					def Player currentUser=userInSession
					newTeam.offsideUser=currentUser
					
					newTeam.setCreationDate(new Date())
					
					def addedCount=0
					Random rand=new Random()
					def usedIds = []
					
					while (addedCount < teamDefaultSize)
					{
					footballerNum=rand.nextInt(totalFootballers+1)
						if (!(usedIds.contains(footballerNum)))
							{
								Footballer selectedFootballer=Footballer.get(footballerNum)
								usedIds.add(footballerNum)
								newTeam.addToFootballers(selectedFootballer)
								addedCount++
								}
						}
					
					if( ! newTeam.save(flush: true)) {
						newTeam.errors.each {
							  println it
						 }
						
					  }
					else
					{
						userInSession.offsideTeams.add(newTeam)
					}
				}
				else
				{
					
				}
			}
		
		
	   
   
	}
    
	def list(Integer max)
	{
		createRandomTeam()
		params.max = Math.min(max ?: 10, 100)
		//[teamInstanceList: Team.list(params), teamInstanceTotal: Team.count()]
		[teamInstanceList: userInSession.offsideTeams, teamInstanceTotal: userInSession.offsideTeams.size()]
		}

}
