package com.offside

class Profile {

	String fullName
	byte[] photo
	String bio
	String homepage

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp

	static belongsTo = Player

	static constraints = {
		fullName(nullable: true)
		bio(nullable: true, maxSize: 1000)
		homepage(url: true, nullable: true)
		photo(nullable: true)
	}

	static scaffold = true
}
