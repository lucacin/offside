package com.offside

class RealTournament {

	String name
	Integer year

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp


	
	static hasMany=[realMatchWeeks:RealMatchWeek, realTeams:RealTeam]

   static constraints = {
		name (blank:false, unique:true)
		year (blank:false)
	}

	static scaffold = true
}
