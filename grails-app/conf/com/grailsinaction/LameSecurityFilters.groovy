package com.grailsinaction

import com.offside.Player;
import com.offside.PlayerController
import javax.servlet.http.HttpSession
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession

class LameSecurityFilters {
	
	// Session parameter is needed
	public void setAuthentication (HttpSession session,Player player) {		
		session.player = player
		session.authenticated=true
	}
	
	// Session parameter is needed
	private boolean isAuthenticatedAccess(HttpSession session) {
		return session.player != null && session.authenticated ==true
	}

	def filters = {
		notAuthenticatedActions(controller:'player', action:'(login|register|forgotPassword)') {
			before = {
				if (this.isAuthenticatedAccess(session)) {
					redirect (uri: '/player/home')
					return false
				}
			}
			after = { Map model ->
			}
			afterView = { Exception e ->
			}
		}

		authenticatedActions(controller:'player', action:'(home|logout)') {
			before = {
				if (!this.isAuthenticatedAccess(session)) {
					def playerController = new PlayerController()
					flash.KOMessage = playerController.message(code: "error.notauthenticated.player")					
					redirect (uri: '/')
					return false 
				}
			}
			after = { Map model ->
			}
			afterView = { Exception e ->
			}
		}
	}
}
