<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="layout" content="main" />
<title><g:message code="player.home.tab.title" /></title>
</head>
<body id="body">
	<g:if test="${session?.authenticated}">
		<h1>
			<g:message code="player.home.wellcome"
				args="${[session?.player?.login]}" />
		</h1>

		<div id="optionsPane">
			<div id="profilePane">
				<g:message code="player.home.options.profile" />
				<!-- 
				<g:link controller="player" action="profile">
					<g:message code="player.home.options.profile" />
				</g:link>
				-->
			</div>
			<div id="tournamentsPane">
				<g:message code="player.home.options.tournaments" />
				<!-- 
				<g:link controller="tournament" action="index">
					<g:message code="player.home.options.tournaments" />
				</g:link> 
				-->
			</div>
			<div id="logout">
				<g:link controller="player" action="logout">
					<g:message code="player.home.options.logout" />
				</g:link>
			</div>

		</div>
	</g:if>
</body>
</html>